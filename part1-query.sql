------ 1. How can you produce a list of the start times for bookings by members named 'David Farrell'?--------------

SELECT m.firstname, m.surname,b.starttime
from cd.members m, cd.bookings b
where m.memid = b.memid and m.surname = 'Farrell' and m.firstname = 'David'


----2. How can you produce a list of the start times for bookings for tennis courts, for the date '2012-09-21'?----------
-----Return a list of start time and facility name pairings, ordered by the time.-----------
SELECT f.name , b.starttime as "the time"
FROM cd.facilities f
inner join cd.bookings b
on f.facid  = b.facid
where cast(starttime as date) = '2012-09-21'
order by "the time" asc 

----3. How can you output a list of all members who have recommended another member? ------
----Ensure that there are no duplicates in the list, and that results are ordered by (surname, firstname).-----
SELECT  m.surname ,m.firstname
FROM cd.members m
where m.recommendedby IS NOT NULL

---4. How can you output a list of all members, including the individual who recommended them (if any)?----------
---Ensure that results are ordered by (surname, firstname).--------- --sai
SELECT DISTINCT  surname, firstname
from cd.members 
where  memid in (SELECT recommendedby  FROM cd.members)
order by surname, firstname

---5. How can you produce a list of all members who have used a tennis court? -----------
---Include in your output the name of the court, and the name of the member formatted as a single column----
----Ensure no duplicate data, and order by the member name followed by the facility name.---
SELECT  DISTINCT concat(m.surname,' ', m.firstname) as fullname,f.name
FROM cd.members m  
inner join cd.bookings b 
on m.memid = b.memid
inner join cd.facilities f
on f.facid = b.facid
WHERE  f.name LIKE 'Tennis Court%'
order by fullname

--cách hai : use where
SELECT DISTINCT concat(m.surname,' ', m.firstname) as fullname,f.name 
from cd.bookings b , cd.facilities f, cd.members m
where m.memid = b.memid and f.facid = b.facid and  f.name LIKE 'Tennis Court%'
order by fullname
--------6. How can you produce a list of bookings on the day of 2012-09-14 which will cost the member (or guest) more than $30?
--------Remember that guests have different costs to members (the listed costs are per half-hour 'slot'), and the guest user is always ID 0.
---------Include in your output the name of the facility, the name of the member formatted as a single column, and the cost.
----------Order by descending cost, and do not use any subqueries. 

SELECT f.name, concat(m.surname,' ', m.firstname) as fullname, f.membercost, f.guestcost
FROM cd.facilities f
inner join cd.bookings b
on f.facid = b.facid
inner join cd.members m
on m.memid = b.memid
WHERE cast(starttime as date) = '2012-09-21'
and (f.membercost >= 30 or f.guestcost >= 30)
order by f.membercost, f.guestcost


WITH agg AS (SELECT concat(m.surname,' ', m.firstname) as fullname,f.name ,
	b.slots * (
	  CASE
		WHEN m.memid = 0 THEN f.guestcost
		ELSE f.membercost
	  END
	) AS cost
  FROM cd.members m
  INNER JOIN cd.bookings b
  ON m.memid = b.memid
  INNER JOIN cd.facilities f
  ON b.facid = f.facid
  WHERE date(b.starttime) = '2012-09-14'
)
SELECT fullname, name , cost
FROM agg
WHERE cost > 30
order by  cost

---7.How can you output a list of all members, including the individual who recommended them (if any),
---without using any joins? Ensure that there are no duplicates in the list,
----and that each firstname + surname pairing is formatted as a column and ordered.
SELECT  DISTINCT (concat(m.surname,' ', m.firstname)) as fullname ,
	CASE 
		WHEN m.recommendedby is Null  then null
		else concat(n.surname,' ', n.firstname)
	End  as recommendedby
FROM cd.members m, cd.members n
WHERE m.recommendedby IS NULL OR n.recommendedby = m.memid
ORDER BY fullname


----8.How can you produce a list of bookings on the day of 2012-09-14 which will cost the member (or guest) more than $30?
---- Remember that guests have different costs to members (the listed costs are per half-hour 'slot'), and the guest user is always ID 0.
-----Include in your output the name of the facility,
----- the name of the member formatted as a single column, and the cost. Order by descending cost.
 
with thongke as
(SELECT concat(m.surname , '' ,m.firstname)as fullname ,f.name,
CASE
	  WHEN m.memid = 0 THEN b.slots * f.guestcost
	  ELSE b.slots * f.membercost
	END AS cost
from cd.bookings b
join cd.members m
on b.memid = m.memid
join cd.facilities f
on f.facid = b.facid
WHERE cast (starttime as date ) = '2012-09-14')

SELECT  d.fullname,d.name,  d.cost
FROM thongke d
WHERE d.cost > 30 
ORDER BY d.cost DESC















